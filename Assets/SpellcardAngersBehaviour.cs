﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellcardAngersBehaviour : SpellcardBehaviour
{
    public float bulletShowerSpawnDelay;
    public float homingShowerSpawnDelay;
    public float laserShowerSpawnDelay;
    public float bulletShowerVelocity;
    public float homingShowerVelocity;

    protected override void instantiateSpell(int mode){
        if(mode == 1){
            StartCoroutine(bulletShowerSpawnerCoroutine());
        } else if (mode == 2){
            StartCoroutine(homingShowerSpawnerCoroutine());
        } else {
            StartCoroutine(laserShowerSpawnerCoroutine());
        }
    }

    IEnumerator bulletShowerSpawnerCoroutine(){
        yield return new WaitForSeconds(0.5f);
        while (true){
            Vector2 position = transform.position;
            position.x += UnityEngine.Random.Range(-1f, 1f) * 3;
            GameObject bullet = Instantiate(bulletFab, position, transform.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -bulletShowerVelocity);
            
            yield return new WaitForSeconds(bulletShowerSpawnDelay);
        }
    }

    IEnumerator homingShowerSpawnerCoroutine(){
        yield return new WaitForSeconds(0.5f);
        while (true){
            Vector2 position = transform.position;
            position.x += UnityEngine.Random.Range(-1f, 1f) * 3;
            GameObject bullet = Instantiate(bulletFab, position, transform.rotation);
            if (player != null){
                float AngleRad = Mathf.Atan2(bullet.transform.position.y - player.transform.position.y, bullet.transform.position.x - player.transform.position.x);
                float AngleDeg = AngleRad*Mathf.Rad2Deg;
                bullet.GetComponent<Rigidbody2D>().rotation = AngleDeg+90;
                bullet.GetComponent<Rigidbody2D>().velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized*homingShowerVelocity;
            } else {
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -homingShowerVelocity);
            }

            yield return new WaitForSeconds(homingShowerSpawnDelay);
        }
    }

    IEnumerator laserShowerSpawnerCoroutine(){
        yield return new WaitForSeconds(0.5f);
        int flowDir = 1;
        while (true){
            //x = -3.5 ~ 3.5
            for(int i=-4; i <= 4; i++){
                Vector2 position = transform.position;
                position.x += i * 0.8f * flowDir;
                position.y -= 2f;
                Instantiate(laserFab, position, transform.rotation);

                yield return new WaitForSeconds(laserShowerSpawnDelay);
            }

            //y = -4.5 ~ 4.5
            for(int i=-4; i <= 4; i++){
                Vector2 position = transform.position;
                position.y += (i * 1.1f * flowDir) - 1.5f;
                
                GameObject laser = Instantiate(laserFab, position,  Quaternion.Euler(0, 0, 90));

                yield return new WaitForSeconds(laserShowerSpawnDelay);
            }
            flowDir *= -1;
        }
    }
}
