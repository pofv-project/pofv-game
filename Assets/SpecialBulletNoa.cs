﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBulletNoa : EnemyBehaviour
{
    public int missileSpawned; 
    public int missileNumber;
    public float elapsedTime;
    public float delaySpawn;

    void Start()
    {
        missileSpawned = 0;
        elapsedTime = delaySpawn;
        missileNumber = mode;
    }

    // Update is called once per frame
    void Update()
    {
        if (missileSpawned >= missileNumber || player == null){
            Destroy(this.gameObject);
        }

        if (elapsedTime >= delaySpawn && player != null){
            GameObject missile = Instantiate(missileFab, transform.position, Quaternion.identity);
            missile.GetComponent<HomingBullet>().targetObj = player;
            elapsedTime = 0;
            missileSpawned++;
        }    

        elapsedTime += Time.deltaTime;
    }
}
