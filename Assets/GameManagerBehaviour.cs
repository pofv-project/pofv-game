﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class GameManagerBehaviour : MonoBehaviour
{
    public GameObject player1Fab;
    public GameObject player2Fab;

    protected GameObject player1;
    protected GameObject player2;

    protected GameObject player1Spell;
    protected GameObject player2Spell;

    protected GameObject player1SpecialBullet;
    protected GameObject player2SpecialBullet;

    public float altitude1 = 0;
    public float altitude2 = 0;

    public GameObject altitudeText1;
    public GameObject altitudeText2;

    public GameObject pointText1;
    public GameObject pointText2;

    public int MODE2_THRESHOLD = 10;
    public int MODE3_THRESHOLD = 20;
    public int WIN_THRESHOLD = 25;

    public GameObject enemySpawnerFab;
    public GameObject enemySpawner1;
    public GameObject enemySpawner2;

    public GameObject winPanel;
    public GameObject pausePanel;

    public GameObject caution1;
    public GameObject caution2;

    public bool isUpUlti;

    public bool gameResolved = false;
    public bool gamePaused = false;

    public bool player1IsAi = false;
    public bool player2IsAi = false;

    public float AISpeed = 20;

    private List<int> nodeList;

    protected GameObject player1SpellcardSpawn;
    protected GameObject player2SpellcardSpawn;

    public float altitudeTimeoutTimePassed = 0;
    public float altitudeTimeoutValue = 30;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        winPanel.SetActive(false);
        pausePanel.SetActive(false);
        Global global = Global.getInstance();
        // GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        // foreach(GameObject player in players){
        //     int playerNumber = player.GetComponent<PlayerBehaviour>().playerNumber;
        //     if (playerNumber == 1){
        //         player1 = player;
        //         player1Spell = player.GetComponent<PlayerBehaviour>().spellFab;
        //         player1SpecialBullet = player.GetComponent<PlayerBehaviour>().specialBulletFab;
                
        //     } else if (playerNumber == 2){
        //         player2 = player;
        //         player2Spell = player.GetComponent<PlayerBehaviour>().spellFab;
        //         player2SpecialBullet = player.GetComponent<PlayerBehaviour>().specialBulletFab;
        //     }
        // }
        if (global.player1Fab != null){
            player1 = Instantiate(global.player1Fab, new Vector2(-5.5f, -2.75f), Quaternion.identity);
        } else {
            player1 = Instantiate(player1Fab, new Vector2(-5.5f, -2.75f), Quaternion.identity);
        }

        if(global.player1IsAi){
            player1IsAi = true;
        }

        if(global.player2IsAi){
            player2IsAi = true;
        }
        player1Spell = player1.GetComponent<PlayerBehaviour>().spellFab;
        player1SpecialBullet = player1.GetComponent<PlayerBehaviour>().specialBulletFab;
        player1.GetComponent<PlayerBehaviour>().playerNumber = 1;
        player1.transform.SetParent(GameObject.FindGameObjectWithTag("Border1").transform);
        
        if(player1IsAi){
            player1.GetComponent<PlayerBehaviour>().aiInput = true;
            player1.GetComponent<PlayerBehaviour>().playerInput = false;
            player1.GetComponent<PlayerBehaviour>().speed = AISpeed;
            loadAgentToPlayer(player1);
        }

        enemySpawner1 = Instantiate(enemySpawnerFab, transform.position, transform.rotation);
        EnemySpawnerBehaviour enemySpawner1Behaviour = enemySpawner1.GetComponent<EnemySpawnerBehaviour>();
        enemySpawner1Behaviour.player = player1;
        enemySpawner1Behaviour.spawnerNumber = 1;
        enemySpawner1Behaviour.mode = 1;
        enemySpawner1.transform.SetParent(GameObject.FindGameObjectWithTag("Border1").transform);


        if (global.player2Fab != null){
            player2 = Instantiate(global.player2Fab, new Vector2(4.5f, -2.75f), Quaternion.identity);
        } else {
            player2 = Instantiate(player2Fab, new Vector2(4.5f, -2.75f), Quaternion.identity);
        }

        player2Spell = player2.GetComponent<PlayerBehaviour>().spellFab;
        player2SpecialBullet = player2.GetComponent<PlayerBehaviour>().specialBulletFab;
        player2.GetComponent<PlayerBehaviour>().playerNumber = 2;
        player2.transform.SetParent(GameObject.FindGameObjectWithTag("Border2").transform);

        if (player2IsAi){
            player2.GetComponent<PlayerBehaviour>().aiInput = true;
            player2.GetComponent<PlayerBehaviour>().playerInput = false;
            player2.GetComponent<PlayerBehaviour>().speed = AISpeed;
            loadAgentToPlayer(player2);
        }
        
        enemySpawner2 = Instantiate(enemySpawnerFab, transform.position, transform.rotation);
        EnemySpawnerBehaviour enemySpawner2Behaviour = enemySpawner2.GetComponent<EnemySpawnerBehaviour>();
        enemySpawner2Behaviour.player = player2;
        enemySpawner2Behaviour.spawnerNumber = 2;
        enemySpawner2Behaviour.mode = 1;
        enemySpawner2.transform.SetParent(GameObject.FindGameObjectWithTag("Border2").transform);


        StartCoroutine(player1ModeChangeListener());
        StartCoroutine(player2ModeChangeListener());
        StartCoroutine(spawnerRandomizerCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            togglePauseMenu();
            //turnBGMPause();
        }

        if(altitudeTimeoutTimePassed >= altitudeTimeoutValue){
            if(altitude1 < MODE3_THRESHOLD){
                increaseAltitude(1, 5);
            }
            
            if(altitude2 < MODE3_THRESHOLD){
                increaseAltitude(2, 5);
            }

        } else {
            altitudeTimeoutTimePassed += Time.deltaTime;
        }

        if (!gameResolved){
            altitudeText1.GetComponent<TMPro.TextMeshProUGUI>().text =  $"{altitude1*40}";
            altitudeText2.GetComponent<TMPro.TextMeshProUGUI>().text = $"{altitude2*40}";
            pointText1.GetComponent<TMPro.TextMeshProUGUI>().text = $"{player1.GetComponent<PlayerBehaviour>().spellCharge*10}";
            pointText2.GetComponent<TMPro.TextMeshProUGUI>().text = $"{player2.GetComponent<PlayerBehaviour>().spellCharge*10}";
        }
    } /*
    public void turnBGMPause(){
        if(!gameResolved){
            if(gamePaused){
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().Pause();
            }

            else{
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().UnPause();
            }
        }
    }
    */
    public virtual void togglePauseMenu(){
        if(!gameResolved){
            if(!gamePaused){
                
                gamePaused = true;
                Time.timeScale = 0;
                pausePanel.SetActive(true);
            } else {
                //MainCamera.GetComponent<AudioSource>().UnPause();
                gamePaused = false;
                Time.timeScale = 1;
                pausePanel.SetActive(false);
            }
        }
    }

    IEnumerator player1ModeChangeListener(){
        yield return new WaitWhile(() => altitude1 < MODE2_THRESHOLD);
        changeMode(2, 1);

        yield return new WaitWhile(() => altitude1 < MODE3_THRESHOLD);
        changeMode(3, 1);
        
        yield return new WaitWhile(() => altitude1 < WIN_THRESHOLD);
        if(!gameResolved){
            Destroy(enemySpawner1);
            Destroy(enemySpawner2);
            Destroy(player2);
            if(player1SpellcardSpawn != null){
                Destroy(player1SpellcardSpawn);
            }
            if(player2SpellcardSpawn != null){
                Destroy(player2SpellcardSpawn);
            }
            GameObject.FindGameObjectWithTag("Border1").GetComponent<BorderBehaviour>().clearAllEnemyAndEnemyBullet();
            GameObject.FindGameObjectWithTag("Border2").GetComponent<BorderBehaviour>().clearAllEnemyAndEnemyBullet();
            gameResolved = true;
            Win(1);
        }
    }

    IEnumerator player2ModeChangeListener(){
        yield return new WaitWhile(() => altitude2 < MODE2_THRESHOLD);
        changeMode(2, 2);

        yield return new WaitWhile(() => altitude2 < MODE3_THRESHOLD);
        changeMode(3, 2);

        yield return new WaitWhile(() => altitude2 < WIN_THRESHOLD);
        if(!gameResolved){
            Destroy(enemySpawner1);
            Destroy(enemySpawner2);
            Destroy(player1);
            if(player1SpellcardSpawn != null){
                Destroy(player1SpellcardSpawn);
            }
            if(player2SpellcardSpawn != null){
                Destroy(player2SpellcardSpawn);
            }
            GameObject.FindGameObjectWithTag("Border1").GetComponent<BorderBehaviour>().clearAllEnemyAndEnemyBullet();
            GameObject.FindGameObjectWithTag("Border2").GetComponent<BorderBehaviour>().clearAllEnemyAndEnemyBullet();
            gameResolved = true;
            Win(2);
        }
    }

    IEnumerator spawnerRandomizerCoroutine(){
        while (true){
            EnemySpawnerBehaviour.randDegree = UnityEngine.Random.Range(0, 181);
            EnemySpawnerBehaviour.weakEnemySpawnIndex = UnityEngine.Random.Range(0, 6);
            EnemySpawnerBehaviour.strongEnemySpawnIndex = UnityEngine.Random.Range(0, 3);
            yield return new WaitForSeconds(1f);
        }
    }

    protected virtual void Win(int playerNumber)
    {
        winPanel.SetActive(true);
        GameObject winTMP = GameObject.Find("WinText");
        winTMP.GetComponent<TMPro.TextMeshProUGUI>().text = $"Player {playerNumber} Win!";
        if (player1 != null){
            player1.GetComponent<PlayerBehaviour>().playerInput = false;
        } else if (player2 != null) {
            player2.GetComponent<PlayerBehaviour>().playerInput = false;
        }
    }

    public void backToMainMenu(){
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;
    }

    void changeMode(int mode, int playerNumber){
        if (playerNumber == 1){
            enemySpawner1.GetComponent<EnemySpawnerBehaviour>().mode = mode;
        } else if (playerNumber == 2){
            enemySpawner2.GetComponent<EnemySpawnerBehaviour>().mode = mode;
        }
    }

    public void spawnSpellCard(int playerNumber){
        if (playerNumber == 1){
            // GameObject.FindGameObjectWithTag("Border1").GetComponent<BorderBehaviour>().clearAllEnemyAndEnemyBullet();
            if(enemySpawner2 != null){
                if (player1SpellcardSpawn == null){
                    StartCoroutine(spellPause(caution2));
                    player1SpellcardSpawn = enemySpawner2.GetComponent<EnemySpawnerBehaviour>().spawnSpell(player1Spell);
                } else {
                    int prevMode = player1SpellcardSpawn.GetComponent<EnemyBehaviour>().mode;
                    Destroy(player1SpellcardSpawn);
                    StartCoroutine(spellPause(caution2));
                    player1SpellcardSpawn = enemySpawner2.GetComponent<EnemySpawnerBehaviour>().spawnSpell(player1Spell);
                    player1SpellcardSpawn.GetComponent<EnemyBehaviour>().mode = prevMode + 1;
                }
            }


        } else if (playerNumber == 2){
            // GameObject.FindGameObjectWithTag("Border2").GetComponent<BorderBehaviour>().clearAllEnemyAndEnemyBullet();
            if(enemySpawner1 != null){
                if (player2SpellcardSpawn == null){
                    StartCoroutine(spellPause(caution1));
                    player2SpellcardSpawn = enemySpawner1.GetComponent<EnemySpawnerBehaviour>().spawnSpell(player2Spell);
                } else {
                    int prevMode = player2SpellcardSpawn.GetComponent<EnemyBehaviour>().mode;
                    Destroy(player2SpellcardSpawn);
                    StartCoroutine(spellPause(caution1));
                    player2SpellcardSpawn = enemySpawner1.GetComponent<EnemySpawnerBehaviour>().spawnSpell(player2Spell);
                    player2SpellcardSpawn.GetComponent<EnemyBehaviour>().mode = prevMode + 1;
                }
            }
        }
        isUpUlti = true;
    }

    public void spawnSpecialBullet(int playerNumber){
        if (playerNumber == 1){
            if(enemySpawner2 != null){ //primitive error checking. kalau udah implement win condition harusnya gak usah dihandle
                enemySpawner2.GetComponent<EnemySpawnerBehaviour>().spawnSpecialBullet(player1SpecialBullet);
            }

        } else if (playerNumber == 2){
            if(enemySpawner1 != null){
                enemySpawner1.GetComponent<EnemySpawnerBehaviour>().spawnSpecialBullet(player2SpecialBullet);
            }
        }
    }

    public void increaseAltitude(int playerNumber, int value){
        if (playerNumber == 1){
            altitude2 += value;
        } else if (playerNumber == 2){
            altitude1 += value;
        }
        altitudeTimeoutTimePassed = 0;
    }

    public void loadAgentToPlayer(GameObject playerObj){
        StreamReader sr = new StreamReader(Path.Combine(Application.streamingAssetsPath, "AI/agent.json"));
        string json = sr.ReadLine();
        saveFile saveObj = JsonUtility.FromJson<saveFile>(json);
        sr.Close();

        nodeList = saveObj.nodes;
        PlayerBehaviour player = playerObj.GetComponent<PlayerBehaviour>();
        player.agent = new NeuralNetwork(true, nodeList);
        player.maxBullet = (nodeList[0] - 2) / 4;

        int biasIndex = 0;
        int weightIndex = 0;

        // layer loop
        for (int i = 0; i < nodeList.Count; i++)
        {
            for (int j = 0; j < nodeList[i]; j++)
            {
                player.agent.biases[i][j] = double.Parse(saveObj.biases[biasIndex++]);
                if (nodeList.Count - i != 1)
                {
                    for (int k = 0; k < nodeList[i + 1]; k++)
                    {
                        player.agent.weights[i][j][k] = double.Parse(saveObj.weights[weightIndex++]);
                    }
                }
            }
        }
    }

    public void saveAgentFromPlayer(GameObject playerObj){
        List<string> fitness = new List<string>();
        List<string> weights = new List<string>();
        List<string> biases = new List<string>();
        PlayerBehaviour player = playerObj.GetComponent<PlayerBehaviour>();

        fitness.Add(string.Format("{0:N5}", player.agent.fitness));

        // layer loop
        for(int i = 0; i < nodeList.Count; i++)
        {
            for(int j = 0; j < nodeList[i]; j++)
            {
                biases.Add(string.Format("{0:N5}", player.agent.biases[i][j]));
                if(nodeList.Count-i != 1)
                {
                    for (int k = 0; k < nodeList[i+1]; k++)
                    {
                        weights.Add(string.Format("{0:N5}", player.agent.weights[i][j][k]));
                    }
                }
            }
        }

        saveFile saveObj = new saveFile(0, 0, nodeList, fitness, weights, biases);
        String json = JsonUtility.ToJson(saveObj);
        StreamWriter sw = new StreamWriter(Application.dataPath + "/AI/save.json");
        sw.WriteLine(json);
        sw.Close();
    }

    IEnumerator spellPause(GameObject caution){
        caution.SetActive(true);
        caution.GetComponent<AudioSource>().Play();
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(0.5f);
        Time.timeScale = 1;
        caution.SetActive(false);
    }
}

[Serializable()]
public class saveFile
{
    public int generation;
    public int agentSize;
    public List<int> nodes;
    public List<string> fitness;
    public List<string> weights;
    public List<string> biases;

    public saveFile(int generation, int agentSize, List<int> nodes, List<string> fitness, List<string> weights, List<string> biases)
    {
        this.generation = generation;
        this.agentSize = agentSize;
        this.nodes = nodes;
        this.fitness = fitness;
        this.weights = weights;
        this.biases = biases;
    }
}
