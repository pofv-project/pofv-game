﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    public int currPlayerChoose = 1;
    public GameObject playerVersusTMP;
    public GameObject playerControllingVersusTMP;
    public GameObject versusStartButton;
    private Global global;

    private void Start() {
        global = Global.getInstance();
        resetGlobalVars();
    }

    public void Story()
    {
        SceneManager.LoadScene("StoryMegu1");        
    }

    public void StoryK()
    {
        SceneManager.LoadScene("StoryKiara1"); 
    }

    public void Versus()
    {
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        SceneManager.LoadScene("VersusScene");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void setGlobalPlayerGameObject(GameObject playerObject){
        if (currPlayerChoose == 1){
            global.player1Fab = playerObject;
            playerVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text = "Player 1: " + playerObject.name + "\n";

        } else if (currPlayerChoose == 2){
            global.player2Fab = playerObject;
            playerVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text += "Player 2: " + playerObject.name;
            versusStartButton.SetActive(true);
        }
    }

    public void setVersusControllingText(){
        if (global.player1IsAi){
            playerControllingVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text = "Player 1: AI\n";
        } else {
            playerControllingVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text = "Player 1: Human\n";
        }

        if (global.player2IsAi){
            playerControllingVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text += "Player 2: AI\n";
        } else {
            playerControllingVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text += "Player 2: Human\n";
        }
    }

    public void incrementCurrPlayerChoose(){
        currPlayerChoose++;
    }

    public void resetGlobalVars(){
        global.resetVarsToDefault();
    }

    public void resetCurrPlayerChoose(){
        currPlayerChoose = 1;
        global.player1Fab = null;
        global.player2Fab = null;
        playerVersusTMP.GetComponent<TMPro.TextMeshProUGUI>().text = " ";
        versusStartButton.SetActive(false);
    }

    public void setPlayer1IsAi(bool value){
        global.setPlayer1IsAi(value);
    }

    public void setPlayer2IsAi(bool value){
        global.setPlayer2IsAi(value);
    }
}
