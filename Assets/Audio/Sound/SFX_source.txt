spawn-bullet: http://wingless-seraph.net/se/enemy-attack.wav (modified)
activate-spell: http://wingless-seraph.net/se/skill.wav
blaster-spell: https://freesound.org/people/jalastram/sounds/362468/ (modified)
caution: https://freesound.org/people/plasterbrain/sounds/242856/
enemy-destroyed: http://wingless-seraph.net/se/damage01.wav
explosion: https://freesound.org/people/ZoshP/sounds/541795/
laser-shoot: http://wingless-seraph.net/se/battle-start.wav (modified)
menu-sfx1: http://wingless-seraph.net/se/save-01.wav
menu-sfx2: http://wingless-seraph.net/se/kettei-01.wav
shoot-sfx: http://wingless-seraph.net/se/charge2.wav