﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBulletKiara : EnemyBehaviour
{
    public int laserSpawned; 
    public int laserNumber;
    public float elapsedTime;
    public float delaySpawn;
    // Start is called before the first frame update
    void Start()
    {
        laserSpawned = 0;
        elapsedTime = delaySpawn;
        laserNumber = mode;
    }

    // Update is called once per frame
    void Update()
    {
        if (laserSpawned >= laserNumber || player == null){
            Destroy(this.gameObject);
        }

        if (elapsedTime >= delaySpawn && player != null){
            shootLaserTracking();
            elapsedTime = 0;
            laserSpawned++;
        }    

        elapsedTime += Time.deltaTime;
    }
}
