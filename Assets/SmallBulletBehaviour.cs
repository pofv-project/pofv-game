﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBulletBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public float velocityX;
    public float velocityY;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         transform.position += new Vector3(velocityX*Time.deltaTime, velocityY*Time.deltaTime, 0);
    }

    private void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
