﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawnerBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject bullet;
    public float velocity;
    public float elapsedTime;
    public float timeout;
    void Start()
    {   
    }

    // Update is called once per frame
    void Update()
    {
        if (elapsedTime >= timeout){
            expandBullets();
        }
        elapsedTime += Time.deltaTime;
    }

    private void expandBullets(){
        if (bullet != null){
            for (int i=0;i <= 36;i++){
                var degree=i*10;
                GameObject smallBullet = Instantiate(bullet, transform.position, Quaternion.identity);
                smallBullet.GetComponent<SmallBulletBehaviour>().velocityX = velocity*Mathf.Cos((degree * Mathf.PI)/180);
                smallBullet.GetComponent<SmallBulletBehaviour>().velocityY = velocity*Mathf.Sin((degree * Mathf.PI)/180);
            }
        }

        Destroy(this.gameObject);
    }
}
