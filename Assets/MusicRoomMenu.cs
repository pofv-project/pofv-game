﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MusicRoomMenu : MonoBehaviour
{
    public AudioSource audioSource;
    public TMPro.TextMeshProUGUI descTMP;
    public TMPro.TextMeshProUGUI arrTMP;
    public TMPro.TextMeshProUGUI compTMP;
    public TMPro.TextMeshProUGUI nowPlayingTMP;

    public void changeAudio(AudioClip clip){
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void changeDescription(string description){
        descTMP.text = description;
    }

    public void changeArrangement(string arrangement){
        arrTMP.text = $"Arrangement: {arrangement}";
    }

    public void changeComposition(string composition){
        compTMP.text = $"Composition: {composition}";
    }

    public void changeNowPlaying(string nowPlaying){
        nowPlayingTMP.text = $"Now playing: {nowPlaying}";
    }
}
