using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Node
{
    public double value;

    public Node()
    {
        this.value = 0;
    }

    public void set_sigmoid(double value)
    {

        double k = Math.Exp(value);
        double res = k / (1.0f + k);
        if(Double.IsNaN(res)){
            res = 0;
        }

        this.set_value(res);
    }

    public void set_tanh(double value)
    {
        this.set_value(Math.Tanh(value));
    }

    public void set_relu(double value, double coefficient=0.02)
    {
        if (value > 0) this.set_value(coefficient * value);
        else this.set_value(0);
    }

    public void set_lrelu(double value, double coefficient=0.02, double lCoefficient=0.1)
    {
        set_value(Math.Max(value * coefficient * lCoefficient, value * coefficient));
    }

    public void set_value(double value)
    {
        this.value = value;
        if (this.value > 1) this.value = 1;
        else if (this.value < -1) this.value = -1;
    }
}

public class NeuralNetwork: IComparable<NeuralNetwork>
{
    public List<List<Node>> layers;
    public List<List<List<double>>> weights;
    public List<List<double>> biases;

    public double fitness = 0;
    public int name;
    public static int name_count = 0;

    public NeuralNetwork(bool randomize, List<int> input)
    {
        this.name = NeuralNetwork.name_count++;
        // UnityEngine.Random rnd = new UnityEngine.Random();

        this.layers = new List<List<Node>>();
        this.weights = new List<List<List<double>>>();
        this.biases = new List<List<double>>();

        for (int i=0; i<input.Count; i++)
        {
            this.layers.Add(new List<Node>());
            this.weights.Add(new List<List<double>>());
            this.biases.Add(new List<double>());

            for (int j=0; j<input[i]; j++)
            {
                this.weights[i].Add(new List<double>());
                this.layers[i].Add(new Node());
                if (randomize)
                {
                    this.biases[i].Add(UnityEngine.Random.Range(-5f, 5f));
                }
                else
                {
                    this.biases[i].Add(0);
                }

                if (input.Count-i != 1)
                {
                    for (int k=0; k<input[i+1]; k++)
                    {
                        if (randomize)
                        {
                            this.weights[i][j].Add(UnityEngine.Random.Range(-5f, 5f));
                        }
                        else
                        {
                            this.weights[i][j].Add(0);
                        }
                        
                    }
                }
            }
        }
    }

    public void feedForward(double[] input)
    {
        for (int i=0; i<this.layers[0].Count; i++)
        {
            this.layers[0][i].set_value(input[i]);
        }

        for (int i=1; i<this.layers.Count; i++)
        {
            for (int j=0; j<this.layers[i].Count; j++)
            {
                double value = 0;
                for (int k=0; k<this.layers[i-1].Count; k++)
                {
                    value += this.layers[i-1][k].value * this.weights[i-1][k][j];  
                }
                value += this.biases[i][j];
                this.layers[i][j].set_tanh(value);
            }
        }
    }

    public double getValues()
    {
        int last_layer = this.layers.Count - 1;
        double value = 0;
        for (int i=0; i<this.layers[last_layer].Count; i++)
        {
            value += this.layers[last_layer][i].value;
        }

        return value;
    }

    public int CompareTo(NeuralNetwork other)
    {
        if (other == null) return 1;
        if (fitness > other.fitness)
            return -1;
        else if (fitness < other.fitness)
            return 1;
        else
            return 0;
    }

    public NeuralNetwork mate(List<int> arr, NeuralNetwork other)
    {
        double mutate = 0.95f;
        NeuralNetwork child = new NeuralNetwork(false, arr);
        for (int i=0; i<weights.Count; i++)
        {
            for(int j=0; j<weights[i].Count; j++)
            {
                double mutationChance = UnityEngine.Random.Range(0f, 1f);
                if (j%2 == 0)
                {
                    child.biases[i][j] = this.biases[i][j];
                }
                else
                {
                    child.biases[i][j] = other.biases[i][j];
                }
                if (mutationChance > mutate)
                {
                    child.biases[i][j] = UnityEngine.Random.Range(-3f, 3f);
                }

                for (int k=0; k<weights[i][j].Count; k++)
                {
                    mutationChance = UnityEngine.Random.Range(0f, 1f);
                    if (k%2 == 0)
                    {
                        child.weights[i][j][k] = this.weights[i][j][k];
                    }
                    else
                    {
                        child.weights[i][j][k] = other.weights[i][j][k];
                    }
                    if (mutationChance > mutate)
                    {
                        child.weights[i][j][k] += UnityEngine.Random.Range(-3f, 3f);
                    }
                }
            }
        }
        return child;
    }

    public NeuralNetwork randomize(List<int> arr)
    {
        double mutate = 0.90;
        NeuralNetwork result = new NeuralNetwork(false, arr);
        for (int i = 0; i < weights.Count; i++)
        {
            for (int j = 0; j < weights[i].Count; j++)
            {
                double increment = 0;
                double mutationChance = UnityEngine.Random.Range(0f, 1f);
                if(mutationChance > mutate)
                {
                    increment = UnityEngine.Random.Range(-3f, 3f);
                }
                result.biases[i][j] = this.biases[i][j] + increment;

                for (int k = 0; k < weights[i][j].Count; k++)
                {
                    increment = 0;
                    mutationChance = UnityEngine.Random.Range(0f, 1f);
                    if (mutationChance > mutate)
                    {
                        increment = UnityEngine.Random.Range(-3f, 3f);
                    }
                    result.weights[i][j][k] = this.weights[i][j][k] + increment;
                }
            }
        }
        return result;
    }
}