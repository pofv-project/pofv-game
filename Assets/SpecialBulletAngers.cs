﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBulletAngers : EnemyBehaviour
{
    public int bulletNumber;
    public int homingNumber;
    public int laserNumber;

    public float bulletSpawnDelay;
    public float homingSpawnDelay;
    public float laserSpawnDelay;

    public float bulletVelocity;
    public float homingVelocity;
    
    private float elapsedTime;
    private float spawnDelay;
    private int spawnNumber;
    private int bulletSpawned;

    void Start()
    {
        bulletSpawned = 0;

        if (mode == 1){
            spawnDelay = bulletSpawnDelay;
            spawnNumber = bulletNumber;
        } else if (mode == 2){
            spawnDelay = homingSpawnDelay;
            spawnNumber = homingNumber;
        } else {
            spawnDelay = laserSpawnDelay;
            spawnNumber = laserNumber;
        }

        elapsedTime = spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        if (bulletSpawned >= spawnNumber || player == null){
            Destroy(this.gameObject);
        }

        if (elapsedTime >= spawnDelay){
            Vector2 position = transform.position;
            position.x += UnityEngine.Random.Range(-1f, 1f) * 3;
            
            if (mode == 1){
                GameObject bullet = Instantiate(bulletFab, position, transform.rotation);
                bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -bulletVelocity);

            } else if (mode == 2){
                GameObject bullet = Instantiate(bulletFab, position, transform.rotation);
                if (player != null){
                    bullet.GetComponent<Rigidbody2D>().velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized*homingVelocity;
                } else {
                    bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -homingVelocity);
                }

            } else {
                position.y -= 4f;
                Instantiate(laserFab, position, transform.rotation);
            }

            elapsedTime = 0;
            bulletSpawned++;
        }

        elapsedTime += Time.deltaTime;
    }
}
