﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayfieldCleanerBehaviour : MonoBehaviour
{
    public int bulletLayer;
    public int expandSpeed;
    private Vector3 scaleChange;
    public float maxScale;
    // Update is called once per frame
    private void Start() {
        scaleChange = new Vector3(expandSpeed, expandSpeed, 0);
    }

    void Update()
    {
        if(transform.localScale.x > maxScale){
            Destroy(gameObject);
        }
        transform.localScale += Time.deltaTime*scaleChange;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Bullet" && other.gameObject.layer == bulletLayer){
            Destroy(other.gameObject);
        } else if (other.tag == "Enemy" && other.transform.parent == transform.parent){
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.tag == "Bullet" && other.gameObject.layer == bulletLayer){
            Destroy(other.gameObject);
        } else if (other.tag == "Enemy" && other.transform.parent == transform.parent){
            Destroy(other.gameObject);
        }
    }
}
