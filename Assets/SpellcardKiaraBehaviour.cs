﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellcardKiaraBehaviour : SpellcardBehaviour
{
    public GameObject horizontalLaser;
    public GameObject verticalLaser;
    public GameObject spinningLaser;
    public float spawnDelay;
    private GameObject spell;

    private int spellCounter = 0;

    protected override void instantiateSpell(int mode){
        StartCoroutine(kiaraSpellSpawnCoroutine());
    }

    IEnumerator kiaraSpellSpawnCoroutine(){
        while (true){
            switch(mode) {
                case 1:
                    spell = spawnHorizontalLaser();
                    break;
                case 2:
                    if (spellCounter == 0){
                        spell = spawnVerticalLaser();
                    } else {
                        spell = spawnHorizontalLaser();
                    }
                    spell.GetComponent<StaticLaserBehaviour>().player = player;
                    spellCounter = (spellCounter + 1) % 2;
                    break;
                default:
                    spell = spawnSpinningLaser();
                    break;
            }
            spell.transform.SetParent(this.transform);
            yield return new WaitForSeconds(spawnDelay);
        }
    }

    private GameObject spawnVerticalLaser(){
        Vector2 position = transform.position;
        position.y -= 4.5f;
        return Instantiate(verticalLaser, position, Quaternion.identity);
    }

    private GameObject spawnHorizontalLaser(){
        Vector2 position = transform.position;
        position.y -= 4f;
        return Instantiate(horizontalLaser, position, Quaternion.identity);
    }
    
    private GameObject spawnSpinningLaser(){
        Vector2 position = transform.position;
        position.y -= 1f;
        return Instantiate(spinningLaser, position, Quaternion.identity);
    }
}
