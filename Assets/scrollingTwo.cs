﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrollingTwo : MonoBehaviour
{
    public float bgSpeed;
    public GameObject bg;
    public GameObject bg2;

    private int mode;

    // Start is called before the first frame update
    void Start()
    {
        mode = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        bg.GetComponent<Transform>().position -= new Vector3((float) 0, (float) bgSpeed * Time.deltaTime, (float) 0);
        bg2.GetComponent<Transform>().position -= new Vector3((float) 0, (float) bgSpeed * Time.deltaTime, (float) 0);

        if (bg.GetComponent<Transform>().position.y <= -11.16){
            bg.GetComponent<Transform>().position = new Vector3((float) 0, (float) 11, (float) 336.9118);
        }
        if (bg2.GetComponent<Transform>().position.y <= -11.16){
            bg2.GetComponent<Transform>().position = new Vector3((float) 0, (float) 11, (float) 336.9118);
        }
    }
}
