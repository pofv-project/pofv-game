﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrolling : MonoBehaviour
{
    public float bgSpeed;
    public GameObject bg;

    private int mode;

    // Start is called before the first frame update
    void Start()
    {
        mode = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (mode == 1){
            bg.GetComponent<Transform>().position += new Vector3((float) 0, (float) bgSpeed, (float) 0);
        }
        else {
            bg.GetComponent<Transform>().position -= new Vector3((float) 0, (float) bgSpeed, (float) 0);
        }

        if (bg.GetComponent<Transform>().position.y <= 1 && mode == 0){
            mode = 1;
        }
        if (bg.GetComponent<Transform>().position.y >= 2.3 && mode == 1){
            mode = 0;
        }
    }
}
