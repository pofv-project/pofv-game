﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    public bool playerInput = false;
    // Border penalty is still broken on the CollisionStay
    public bool borderPenalty = false;
    public bool isDead = false;
    public double fitness;

    private float playerSizeX;
    private float playerSizeY;

    public NeuralNetwork agent;
    public bool circleDetection = true;
    public int indices = 36;
    public float circleSize = 0.05f;
    public float detectRadius = 2.5f;
    public bool debugRay = false;


    private Rigidbody2D rb2d;
    private Vector2 move;
    private Vector2 initPosition;

    public GameObject bulletFab;
    public GameObject spellFab;
    public GameObject specialBulletFab;

    public Animator animator;

    private float shootTimePassed;
    public float shootDelay;

    public int playerNumber = 1;

    private RectTransform border;
    private Rect borderRect;
    private float leftBorder;
    private float rightBorder;
    private float upBorder;
    private float downBorder;
    
    private KeyCode upKey;
    private KeyCode downKey;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode focusKey;
    private KeyCode shootKey;
    private KeyCode spellKey;

    public float spellCharge;
    public float spellChargeMaxValue = 15;
    public int spellActivateThreshold;
    public float spellActivateDelay;
    public float spellActivateTimePassed;

    public int hp;

    public float invincibleDelay = 3;
    public float invincibleTimePassed = 3;
    public float cantMoveDelay = 1;

    public bool aiInput;

    public int maxBullet;
    private double borderDiagonalDist;
    
    public Animator grazeRingAnimator;

    private bool hitBullet = false;
    public float startRadius = 0.5f;

    private int bulletLayer;
    public GameObject playfieldCleanerFab;
    // void Demo()
    // {
    //     // Debug
    //     // S Matrix
    //     Matrix s = new Matrix(5, 3);
    //     for (int i = 0; i < s.y; i++)
    //     {
    //         for (int j = 0; j < s.x; j++)
    //         {
    //             s.insert(j, i, i + j);
    //         }

    //     }
    //     string tmp = "\n";
    //     for (int i = 0; i < s.y; i++)
    //     {
    //         for (int j = 0; j < s.x; j++)
    //         {
    //             tmp += s.get(j, i) + " ";
    //         }
    //         tmp += "\n";
    //     }
    //     Debug.Log(tmp);

    //     // T Matrix
    //     Matrix t = new Matrix(1, 5);
    //     for (int i = 0; i < t.y; i++)
    //     {
    //         for (int j = 0; j < t.x; j++)
    //         {
    //             t.insert(j, i, 3 * i);
    //         }
    //     }
    //     tmp = "\n";
    //     for (int i = 0; i < t.y; i++)
    //     {
    //         for (int j = 0; j < t.x; j++)
    //         {
    //             tmp += t.get(j, i) + " ";
    //         }
    //         tmp += "\n";
    //     }
    //     Debug.Log(tmp);

    //     // New matrix mult
    //     Matrix test = Matrix.multiplyMatrix(s, t);
    //     tmp = "\n";
    //     for (int i = 0; i < test.y; i++)
    //     {
    //         for (int j = 0; j < test.x; j++)
    //         {
    //             tmp += test.get(j, i) + " ";
    //         }
    //         tmp += "\n";
    //     }
    //     Debug.Log(tmp);
    // }

    public void setMovement(double vertical, double horizontal)
    {
        move = new Vector2((float)vertical, (float)horizontal);
    }

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        initPosition = transform.position;
        playerSizeX = GetComponent<SpriteRenderer>().bounds.size.x;
        playerSizeY = GetComponent<SpriteRenderer>().bounds.size.y;
        invincibleTimePassed = invincibleDelay;
        animator = GetComponent<Animator>();
        animator.SetFloat("inv_time", 3);
        GameObject borderObj = null;

        Dictionary<string, KeyCode> controlKeyDict = Global.getInstance().controlKeyDict;
        if (playerNumber == 1){
            borderObj = GameObject.FindGameObjectWithTag("Border1");
            bulletLayer = 8;

            leftKey = controlKeyDict["player1LeftKey"];
            rightKey = controlKeyDict["player1RightKey"];
            upKey = controlKeyDict["player1UpKey"];
            downKey = controlKeyDict["player1DownKey"];
            focusKey = controlKeyDict["player1FocusKey"];
            shootKey = controlKeyDict["player1ShootKey"];
            spellKey = controlKeyDict["player1SpellKey"];
        } else if (playerNumber == 2){
            borderObj = GameObject.FindGameObjectWithTag("Border2");
            bulletLayer = 11;

            leftKey = controlKeyDict["player2LeftKey"];
            rightKey = controlKeyDict["player2RightKey"];
            upKey = controlKeyDict["player2UpKey"];
            downKey = controlKeyDict["player2DownKey"];
            focusKey = controlKeyDict["player2FocusKey"];
            shootKey = controlKeyDict["player2ShootKey"];
            spellKey = controlKeyDict["player2SpellKey"];
        }
        
        border = borderObj.GetComponent<RectTransform>();
        borderRect = border.rect;
        // leftBorder = border.xMin + playerSizeX/2;
        // rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - playerSizeX/2;
        // upBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + playerSizeY/2;
        // downBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - playerSizeY/2;
        leftBorder = border.position.x + borderRect.xMin;
        rightBorder = border.position.x + borderRect.xMax;
        upBorder = border.position.y + borderRect.yMax;
        downBorder = border.position.y + borderRect.yMin;
        borderDiagonalDist = calculateDiagonal(rightBorder-leftBorder, upBorder-downBorder);
        if(aiInput){
            StartCoroutine(aiMovementListener());
        }
        //StartCoroutine(testRunAt200ms());
        // Demo();
    }

    void Update()
    {
        if(shootTimePassed < shootDelay){
            shootTimePassed += Time.deltaTime;
        }
        
        if (invincibleTimePassed < invincibleDelay){
            invincibleTimePassed += Time.deltaTime;
            animator.SetFloat("inv_time", invincibleTimePassed);
        }
        
        if (spellActivateTimePassed < spellActivateDelay){
            spellActivateTimePassed += Time.deltaTime;
        }

        grazeRingAnimator.SetBool("focus_input", Input.GetKey(focusKey));
        clampPlayerMovement();
//        fitness = agent.fitness;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float currSpeed = speed;
        if(invincibleTimePassed > cantMoveDelay){
            if (playerInput){
                move = Vector2.zero;
                if(Input.GetKey(rightKey)) move.x += 1;
                if(Input.GetKey(leftKey)) move.x -= 1;
                if(Input.GetKey(upKey)) move.y += 1;
                if(Input.GetKey(downKey)) move.y -= 1;


                if (Input.GetKey(focusKey)){
                    currSpeed *= 0.5f;
                }

                if (Input.GetKey(shootKey) && shootTimePassed >= shootDelay){
                    GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
                    Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
                    bulletRb2d.velocity = new Vector2(0, 10);
                    shootTimePassed = 0;
                }

                if (Input.GetKey(spellKey) && spellCharge >= spellActivateThreshold && spellActivateTimePassed > spellActivateDelay){
                    GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerBehaviour>().spawnSpellCard(playerNumber);
                    spellCharge -= spellActivateThreshold;
                    spellActivateTimePassed = 0;
                    GameObject cleaner = Instantiate(playfieldCleanerFab, transform.position, transform.rotation);
                    cleaner.GetComponent<PlayfieldCleanerBehaviour>().bulletLayer = bulletLayer;
                    cleaner.transform.SetParent(transform.parent);
                }

            } else if (aiInput){
                if (shootTimePassed >= shootDelay){
                    GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
                    Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
                    bulletRb2d.velocity = new Vector2(0, 10);
                    shootTimePassed = 0;
                }
                
                if (spellCharge >= spellActivateThreshold && spellActivateTimePassed > spellActivateDelay){
                    GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerBehaviour>().spawnSpellCard(playerNumber);
                    spellCharge -= spellActivateThreshold;
                    spellActivateTimePassed = 0;
                    GameObject cleaner = Instantiate(playfieldCleanerFab, transform.position, transform.rotation);
                    cleaner.GetComponent<PlayfieldCleanerBehaviour>().bulletLayer = bulletLayer;
                    cleaner.transform.SetParent(transform.parent);
                }
            }
        }


        rb2d.velocity = move*currSpeed;
        animator.SetFloat("Speed_x", move.x);
    }

    void clampPlayerMovement()
    {
        Vector3 position = transform.position;

        position.x = Mathf.Clamp(position.x, leftBorder, rightBorder);
        position.y = Mathf.Clamp(position.y, downBorder, upBorder);

        transform.position = position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet" && invincibleTimePassed >= invincibleDelay && other.gameObject.layer == bulletLayer){
            if(other.GetComponent<MagicLaserBehavior>() != null){
                if(!other.GetComponent<MagicLaserBehavior>().colliderEnabled){
                    return;
                }
            }
            
            getHit();

        } else if (other.gameObject.tag == "Player" || other.gameObject.tag == "PlayerBullet"){
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());

        } else if (other.gameObject.tag == "Border"){
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            // if (borderPenalty) agent.fitness -= 10;
        }
        return;
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.GetComponent<MagicLaserBehavior>() != null && invincibleTimePassed >= invincibleDelay && other.gameObject.layer == bulletLayer)
        {
            if(other.GetComponent<MagicLaserBehavior>().colliderEnabled){
                getHit();
            }
        }
    }

    public void getHit(){
        GetComponent<AudioSource>().Play();
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerBehaviour>().increaseAltitude(playerNumber, 5);
        hp -= 1;
        invincibleTimePassed = 0;
        animator.SetFloat("inv_time", 0);
        resetPosition();
    }

    public void resetPosition(){
        transform.position = new Vector2(border.position.x, downBorder);
        move = new Vector2(0, 0.2f);
        StartCoroutine(stopMovingWhenCanMoveCoroutine());
    }

    IEnumerator stopMovingWhenCanMoveCoroutine(){
        yield return new WaitWhile(() => invincibleTimePassed < cantMoveDelay);
        move = new Vector2(0, 0);
    }

    // private void OnCollisionStay(Collision collision)
    // {
    //     if (collision.gameObject.tag == "Border")
    //     {
    //         if(borderPenalty) agent.fitness -= 1;
    //     }
    // }

    // IEnumerator testRunAt200ms()
    // {
    //     while(true)
    //     {
    //         GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
    //         foreach(GameObject bullet in bullets){
    //             Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
    //             BulletBehaviour bulletBehaviour = bullet.GetComponent<BulletBehaviour>();
    //             Debug.LogFormat("deg = {0}, veloc = {1}, dist = {2}, number = {3}",
    //              Vector2.Angle(bullet.transform.position, gameObject.transform.position),
    //              calculateVelocity(bulletRb2d.velocity.x, bulletRb2d.velocity.y),
    //              Vector2.Distance(bullet.transform.position, gameObject.transform.position), bulletBehaviour.thisCount);
    //         }

    //         yield return new WaitForSeconds(0.2f);
    //     }
    // }

    IEnumerator aiMovementListener()
    {
        while(true)
        {
            if(invincibleTimePassed > cantMoveDelay){
                List<GameObject> bullets = new List<GameObject>();
                foreach(GameObject bullet in GameObject.FindGameObjectsWithTag("Bullet")){
                    if(bullet.transform.position.x >= leftBorder && bullet.transform.position.x <= rightBorder){
                        bullets.Add(bullet);
                    }
                }
                
                inputAi(bullets.ToArray());
                if (hitBullet) feedForwardAi();
                else moveToInit();
            }

            yield return new WaitForFixedUpdate();
        }
    }

    private double calculateVelocity(float vx, float vy){
        return Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
    }

    private void OnBecameInvisible() {
        transform.position = initPosition;
    }

    public void increaseSpellCharge(float value){
        if(invincibleTimePassed > cantMoveDelay){
            spellCharge += value;
            spellCharge = Mathf.Min(spellCharge, spellChargeMaxValue);
        }
    }
    
    public void inputAi(GameObject[] bullets){
        List<BulletObject> bullist = new List<BulletObject>();
        double[] sensors = new double[indices];
        float theta = 360f / indices;
        hitBullet = false;

        for (int i = 0; i < indices; i++)
        {
            Vector2 direction = new Vector2();
            direction.x = Mathf.Cos(Mathf.Deg2Rad * theta * i);
            direction.y = Mathf.Sin(Mathf.Deg2Rad * theta * i);
            //RaycastCommand rayCast = new RaycastCommand(transform.position, direction, detectRadius, 8, 1);

            RaycastHit2D hitObject;
            if (circleDetection) hitObject = Physics2D.CircleCast(transform.position, circleSize, direction, detectRadius, (1 << bulletLayer) + (1 << 9));
            else hitObject = Physics2D.Raycast(transform.position, direction, detectRadius, 1 << bulletLayer, -100, 100);

            if (hitObject) sensors[i] = hitObject.distance;
            else sensors[i] = detectRadius;

            if (hitObject && hitObject.collider.tag == "Bullet") hitBullet = true;

            //Gizmos.DrawLine(transform.position, direction * detectRadius);
            Debug.DrawRay(transform.position, direction * (float)sensors[i], Color.red);
        }

        for (int i = 0; i < bullets.Length; i++)
        {
            GameObject bullet = bullets[i];
            Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
            double distance = Math.Abs(Vector2.Distance(bullet.transform.position, transform.position));

            if (distance > detectRadius) continue;
            hitBullet = true;

            Vector2 bulletRelativePlayer = bullet.transform.position - transform.position;
            double angle = Vector2.SignedAngle(bulletRelativePlayer, Vector2.up) + (theta / 2);
            if (angle < 0)
            {
                angle = angle + 360;
            }
            int ind = (int)Math.Floor(angle / theta);

            if (sensors[ind] > distance) sensors[ind] = distance;
        }

        for (int i = 0; i < indices; i++)
        {
            agent.layers[0][i].set_value(1 - sensors[i] / detectRadius);
        }
    }

    public void feedForwardAi(){
        int lastLayer = agent.layers.Count - 1;
        for (int i = 1; i < lastLayer; i++)
        {
            for (int j = 0; j < agent.layers[i].Count; j++)
            {
                double value = 0;
                for (int k = 0; k < agent.layers[i - 1].Count; k++)
                {
                    value += agent.layers[i - 1][k].value * agent.weights[i - 1][k][j];
                }
                value += agent.biases[i][j];
                agent.layers[i][j].set_tanh(value);
            }
        }

        if (agent.layers[lastLayer].Count == 2)
        {
            // If last layer has 2 neurons
            // Output is (left, right), (up, down)
            for (int j = 0; j < agent.layers[lastLayer].Count; j++)
            {
                double value = 0;
                for (int k = 0; k < agent.layers[lastLayer - 1].Count; k++)
                {
                    value += agent.layers[lastLayer - 1][k].value * agent.weights[lastLayer - 1][k][j];
                }
                value += agent.biases[lastLayer][j];
                agent.layers[lastLayer][j].set_value(value * 0.02);
            }
            setMovement(agent.layers[lastLayer][0].value, agent.layers[lastLayer][1].value);
        }
        else if (agent.layers[lastLayer].Count == 4)
        {
            // if output layer has 4 neurons
            // The output is as follows = { up, down, left, right }
            // With -1 to 0 means inactive and the rest moves at a rate
            for (int j = 0; j < agent.layers[lastLayer].Count; j++)
            {
                double value = 0;
                for (int k = 0; k < agent.layers[lastLayer - 1].Count; k++)
                {
                    value += agent.layers[lastLayer - 1][k].value * agent.weights[lastLayer - 1][k][j];
                }
                value += agent.biases[lastLayer][j];
                agent.layers[lastLayer][j].set_relu(value, 0.04);
            }

            double up = agent.layers[lastLayer][0].value;
            double down = agent.layers[lastLayer][1].value;
            double left = agent.layers[lastLayer][2].value;
            double right = agent.layers[lastLayer][3].value;
            setMovement(right - left, down - up);
        }
    }

    public void moveToInit()
    {
        Vector2 velocity = new Vector2();
        Vector2 startPos = new Vector2(border.position.x, downBorder + 1.5f);
        if (Mathf.Abs(Vector2.Distance(transform.position, startPos)) > startRadius)
        {
            velocity = startPos - (Vector2)transform.position;
            velocity.Normalize();
            velocity = velocity * 0.2f;
        }
        else
        {
            velocity = startPos - (Vector2)transform.position;
            velocity.Normalize();
            velocity = velocity * 0.01f;
        }
        setMovement(velocity.x, velocity.y);
    }

    private double calculateDiagonal(double vx, double vy){
        return Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
    }
}

public class BulletObject: IComparable<BulletObject>
{
    public double danger;
    public double distance;
    public double angle;
    public double bulletAngle;
    public double bulletSpeed;

    public BulletObject(double danger, double distance, double angle, double bulletAngle, double bulletSpeed)
    {
        this.danger = danger;
        this.distance = distance;
        this.angle = angle;
        this.bulletAngle = bulletAngle;
        this.bulletSpeed = bulletSpeed;
    }

    public int CompareTo(BulletObject other)
    {
        if (other == null) return 1;
        if (danger > other.danger)
            return -1;
        else if (danger < other.danger)
            return 1;
        else
            return 0;
    }

    public override String ToString()
    {
        return danger.ToString();
    }
}
