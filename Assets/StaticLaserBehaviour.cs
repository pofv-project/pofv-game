﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticLaserBehaviour : MonoBehaviour
{
    public float timeout;
    private float elapsedTime;
    public GameObject player;
    public MagicLaserBehavior[] laserList;
    // Start is called before the first frame update
    private void Start() {
        elapsedTime = 0;
        if(player != null){
            foreach(MagicLaserBehavior laser in laserList){
                laser.player = player;
            }
        }
    }

    public void changeLayer(){
        foreach(MagicLaserBehavior laser in laserList){
            laser.gameObject.layer = gameObject.layer;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (elapsedTime > timeout){
            Destroy(this.gameObject);
        }
        elapsedTime += Time.deltaTime;
    }
}
