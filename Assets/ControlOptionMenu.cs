﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ControlOptionMenu : MonoBehaviour
{
    public TMPro.TextMeshProUGUI player1LeftText;
    public TMPro.TextMeshProUGUI player1RightText;
    public TMPro.TextMeshProUGUI player1UpText;
    public TMPro.TextMeshProUGUI player1DownText;
    public TMPro.TextMeshProUGUI player1FocusText;
    public TMPro.TextMeshProUGUI player1ShootText;
    public TMPro.TextMeshProUGUI player1SpellText;

    public TMPro.TextMeshProUGUI player2LeftText;
    public TMPro.TextMeshProUGUI player2RightText;
    public TMPro.TextMeshProUGUI player2UpText;
    public TMPro.TextMeshProUGUI player2DownText;
    public TMPro.TextMeshProUGUI player2FocusText;
    public TMPro.TextMeshProUGUI player2ShootText;
    public TMPro.TextMeshProUGUI player2SpellText;

    private Dictionary<string, TMPro.TextMeshProUGUI> controlKeyTextDict;

    private Global global;
    private bool isChangingKey;
    private string keyToChange;

    void Start() {
        global = Global.getInstance();
        controlKeyTextDict = new Dictionary<string, TMPro.TextMeshProUGUI>();
        // This needs refactoring but i can't think a more elegant solution for this without GUI scripting
        controlKeyTextDict.Add("player1LeftKey", player1LeftText);
        controlKeyTextDict.Add("player1RightKey", player1RightText);
        controlKeyTextDict.Add("player1UpKey", player1UpText);
        controlKeyTextDict.Add("player1DownKey", player1DownText);
        controlKeyTextDict.Add("player1FocusKey", player1FocusText);
        controlKeyTextDict.Add("player1ShootKey", player1ShootText);
        controlKeyTextDict.Add("player1SpellKey", player1SpellText);

        controlKeyTextDict.Add("player2LeftKey", player2LeftText);
        controlKeyTextDict.Add("player2RightKey", player2RightText);
        controlKeyTextDict.Add("player2UpKey", player2UpText);
        controlKeyTextDict.Add("player2DownKey", player2DownText);
        controlKeyTextDict.Add("player2FocusKey", player2FocusText);
        controlKeyTextDict.Add("player2ShootKey", player2ShootText);
        controlKeyTextDict.Add("player2SpellKey", player2SpellText);
        isChangingKey = false;
        updateText();
    }

    public void changeKey(string key){
        if(!isChangingKey){
            controlKeyTextDict[key].text = "Press a key...";
            isChangingKey = true;
            keyToChange = key;
        }
    }

    private void OnGUI() {
        if (isChangingKey){
            Event e = Event.current;
            if (e.isKey){
                isChangingKey = false;
                global.controlKeyDict[keyToChange] = e.keyCode;
                updateText();
            }
        }
    }

    public void updateText(){
        player1LeftText.text = global.controlKeyDict["player1LeftKey"].ToString();
        player1RightText.text = global.controlKeyDict["player1RightKey"].ToString();
        player1UpText.text = global.controlKeyDict["player1UpKey"].ToString();
        player1DownText.text = global.controlKeyDict["player1DownKey"].ToString();
        player1FocusText.text = global.controlKeyDict["player1FocusKey"].ToString();
        player1ShootText.text = global.controlKeyDict["player1ShootKey"].ToString();
        player1SpellText.text = global.controlKeyDict["player1SpellKey"].ToString();

        player2LeftText.text = global.controlKeyDict["player2LeftKey"].ToString();
        player2RightText.text = global.controlKeyDict["player2RightKey"].ToString();
        player2UpText.text = global.controlKeyDict["player2UpKey"].ToString();
        player2DownText.text = global.controlKeyDict["player2DownKey"].ToString();
        player2FocusText.text = global.controlKeyDict["player2FocusKey"].ToString();
        player2ShootText.text = global.controlKeyDict["player2ShootKey"].ToString();
        player2SpellText.text = global.controlKeyDict["player2SpellKey"].ToString();
    }

}
