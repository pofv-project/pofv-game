﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour 
{
    public GameObject player1Fab;
    public GameObject player2Fab;
    public bool player1IsAi;
    public bool player2IsAi;
    
    public Dictionary<string, KeyCode> controlKeyDict;

    public bool haveReadDialogue = false;
    private static Global instance;

    void Awake() {
        if (instance == null){
            DontDestroyOnLoad(this.gameObject);
            instance = this;
            setDefaultKey();
        } else if (instance != this){
            Destroy(this.gameObject);
        }
    }

    public static Global getInstance(){
        return instance;
    }

    public void setDefaultKey(){
        controlKeyDict = new Dictionary<string, KeyCode>();
        controlKeyDict.Add("player1LeftKey", KeyCode.A);
        controlKeyDict.Add("player1RightKey", KeyCode.D);
        controlKeyDict.Add("player1UpKey", KeyCode.W);
        controlKeyDict.Add("player1DownKey", KeyCode.S);
        controlKeyDict.Add("player1FocusKey", KeyCode.LeftShift);
        controlKeyDict.Add("player1ShootKey", KeyCode.J);
        controlKeyDict.Add("player1SpellKey", KeyCode.K);

        controlKeyDict.Add("player2LeftKey", KeyCode.LeftArrow);
        controlKeyDict.Add("player2RightKey", KeyCode.RightArrow);
        controlKeyDict.Add("player2UpKey", KeyCode.UpArrow);
        controlKeyDict.Add("player2DownKey", KeyCode.DownArrow);
        controlKeyDict.Add("player2FocusKey", KeyCode.RightControl);
        controlKeyDict.Add("player2ShootKey", KeyCode.Keypad2);
        controlKeyDict.Add("player2SpellKey", KeyCode.Keypad3);
    }

    public void resetVarsToDefault(){
        player1Fab = null;
        player2Fab = null;
        haveReadDialogue = false;
        player1IsAi = false;
        player2IsAi = false;
    }

    public void setPlayer1IsAi(bool value){
        player1IsAi = value;
    }

    public void setPlayer2IsAi(bool value){
        player2IsAi = value;
    }
}