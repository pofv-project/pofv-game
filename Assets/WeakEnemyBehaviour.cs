﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakEnemyBehaviour : EnemyBehaviour
{
    // Update is called once per frame
    void Update() {
        if (hp <= 0){
            destroySelf();
        }

        timePassed += Time.deltaTime;
        if (timePassed >= delay){
            if (mode == 2){
                delay = 0.3f;
                shootBulletTracking();
            } else if (mode == 3){
                delay = 0.1f;
                shootBulletTracking();
            }
            
            timePassed = 0;
        }
    }
}
