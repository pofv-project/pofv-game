﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrongEnemyBehaviour : EnemyBehaviour
{
    void Update() {
        timePassed += Time.deltaTime;
        if (timePassed >= delay){
            if (mode == 1){
                shootBulletRotating();

            } else if (mode == 2){
                delay = 0.2f;
                rotationSpeed = 30;
                shootBulletRotating();
                shootBulletTracking();

            } else if (mode == 3){
                delay = 0.1f;
                rotationSpeed = 30;
                shootBulletRotating();
                shootHomingBullet();
            }
            
            timePassed = 0;
        }
    }

    protected override void destroySelf(){
        int playerNumber = player.GetComponent<PlayerBehaviour>().playerNumber;
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerBehaviour>().spawnSpecialBullet(playerNumber);
        Destroy(this.gameObject);
    }
}
