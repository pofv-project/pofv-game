﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerBehaviour : MonoBehaviour
{
    private RectTransform border;
    public int spawnerNumber = 1;
    public GameObject player;

    public GameObject[] weakEnemyFabList;
    public GameObject[] strongEnemyFabList;

    public int spawnCounter = 0;
    // Start is called before the first frame update

    public float spawnDelay = 2;
    public float spawnTimePassed = 0;
    public int mode = 1;
    public static float randDegree;
    public static int weakEnemySpawnIndex = 0;
    public static int strongEnemySpawnIndex = 0;
    void Start()
    {
        GameObject borderObj = null;
        if (spawnerNumber == 1){
            borderObj = GameObject.FindGameObjectWithTag("Border1");
        } else if (spawnerNumber == 2){
            borderObj = GameObject.FindGameObjectWithTag("Border2");
        }

        border = borderObj.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        spawnTimePassed += Time.deltaTime;
        if (spawnTimePassed >= spawnDelay){
            float xOffset;
            float yOffset;
            weakEnemySpawnIndex =  weakEnemySpawnIndex % weakEnemyFabList.Length;
            strongEnemySpawnIndex = strongEnemySpawnIndex % strongEnemyFabList.Length;
            if(randDegree <= 46){
                xOffset = border.rect.xMin;
                yOffset = Mathf.Sin(2*randDegree * Mathf.Deg2Rad)*border.rect.yMin;
            } else if (randDegree > 45 && randDegree < 130){
                xOffset = Mathf.Cos(2*(randDegree-45) * Mathf.Deg2Rad)*border.rect.xMin;
                yOffset = border.rect.yMin-0.2f;
            } else {
                xOffset = border.rect.xMax;
                yOffset = Mathf.Abs(Mathf.Sin(2*randDegree * Mathf.Deg2Rad))*border.rect.yMin;
            }

            switch(spawnCounter){
                case 0:
                    spawnDelay = spawnStraight(weakEnemyFabList[weakEnemySpawnIndex], randDegree, new Vector2(border.position.x+xOffset, border.position.y-yOffset));
                    break;
                
                case 1:
                    spawnDelay = spawnStraight(weakEnemyFabList[weakEnemySpawnIndex], randDegree, new Vector2(border.position.x+xOffset, border.position.y-yOffset));
                    break;

                case 2:
                    spawnDelay = spawnStraightAndStop(strongEnemyFabList[strongEnemySpawnIndex], 5, 270, new Vector2(border.position.x, border.position.y+1.5f*border.rect.yMax+0.75f));
                    break;
            }

            spawnTimePassed = 0;
            spawnCounter = (spawnCounter + 1) % 4;
        }

    }

    public float spawnStraight(GameObject enemyType, int number, float speed, float direction, Vector2 spawnStartPosition) {
        //direction: 0-90 = kuadran 1, 90-180 = kuadran 4, 180-270 = kuadran 3, 270-360 = kuadran 2.
        float directionRadian = direction * Mathf.Deg2Rad;
        Vector2 velocity = new Vector2(speed*Mathf.Cos(directionRadian), speed*-Mathf.Sin(directionRadian));
        
        StartCoroutine(spawnStraightCoroutine(enemyType, number, speed, velocity, spawnStartPosition));

        return (10/speed)+(number*0.1f);
    }

    public float spawnStraight(GameObject enemyType, float direction, Vector2 spawnStartPosition) {
        return spawnStraight(enemyType, 5, 10, direction, spawnStartPosition);
    }

    IEnumerator spawnStraightCoroutine(GameObject enemyType, int number, float speed, Vector2 velocity, Vector2 spawnStartPosition){
        int counterNumber = 0;
        while (counterNumber < number){
            GameObject enemy = enemySpawnHelper(enemyType, spawnStartPosition, Quaternion.identity);
            enemy.GetComponent<EnemyBehaviour>().player = player;
            enemy.GetComponent<EnemyBehaviour>().mode = mode;
            Rigidbody2D enemyRb2d = enemy.GetComponent<Rigidbody2D>();
            enemyRb2d.velocity = velocity;
            counterNumber++;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public float spawnStraightAndStop(GameObject enemyType, float speed, float direction, Vector2 spawnStartPosition){
        float directionRadian = direction * Mathf.Deg2Rad;
        Vector2 velocity = new Vector2(speed*Mathf.Cos(directionRadian), speed*Mathf.Sin(directionRadian));

        GameObject enemy = enemySpawnHelper(enemyType, spawnStartPosition, Quaternion.identity);
        enemy.GetComponent<EnemyBehaviour>().player = player;
        enemy.GetComponent<EnemyBehaviour>().mode = mode;
        Rigidbody2D enemyRb2d = enemy.GetComponent<Rigidbody2D>();
        enemyRb2d.velocity = velocity;

        enemy.GetComponent<EnemyBehaviour>().startMoveAndStopCoroutine();
        return (10/speed);
    }

    public GameObject spawnSpell(GameObject spellObject){
        Vector2 position = new Vector2(border.position.x, border.position.y*border.rect.yMax);
        // position.y += 3f;
        return enemySpawnHelper(spellObject, position, Quaternion.identity);
        
        // if (spell.GetComponent<SpellcardKiaraBehaviour>() != null){
        //     spell.GetComponent<SpellcardKiaraBehaviour>().mode = mode;
        // }
        // else if (spell.GetComponent<SpellcardMeguBehaviour>() != null){
        //     spell.GetComponent<SpellcardMeguBehaviour>().mode = mode;
        // }
        //spell.GetComponent<TriangleLaser>().timeout = 10;
        //spell.GetComponent<TriangleLaser>().angularSpeed = 1;
    }

    public void spawnSpecialBullet(GameObject bulletObject){
        Vector2 position = new Vector2(border.position.x, border.position.y+border.rect.yMax-0.5f);
        enemySpawnHelper(bulletObject, position, Quaternion.identity);
    }

    public GameObject enemySpawnHelper(GameObject enemyFab, Vector2 position, Quaternion direction){
        GameObject enemy = Instantiate(enemyFab, position, direction);
        enemy.GetComponent<EnemyBehaviour>().player = player;
        enemy.GetComponent<EnemyBehaviour>().mode = mode;
        enemy.transform.SetParent(gameObject.transform.parent);
        return enemy;
    }
}