﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellcardNoaBehaviour : SpellcardBehaviour
{
    public float homingMissileDelay;
    public float bulletRainDelay;
    public float bulletRainAngle;
    public int bulletRainSpawnPoint;
    public float spinningLaserDelay;
    public float spinningLaserRotationSpeed;
    private GameObject spawner;

    override protected void instantiateSpell(int mode){
        //Bug ? atau feature ? 
        //Ketika sebuah gameobject mempunyai parent ke spellcard
        //maka akan terjadi stuck gak gerak samsek ketika laser mempunyai collision enabled
        //jadinya untuk mendelete semua bullet ketika spellcarddnya mati,
        //bikin sebuah dummy parent untuk dijadikan parent, sehingga untuk mendestroy semua bullet ketika spellcardnya mati
        //cukup destroy si dummy parent ini
        spawner = new GameObject("NoaBulletSpawner");

        if (mode >= 1){
            StartCoroutine(bulletRainSpawnerCoroutine());
        }

        if (mode >= 2){
            StartCoroutine(homingMissileSpawnerCoroutine());
        }

        if (mode == 3){
            StartCoroutine(spinningLaserSpawnerCoroutine());
        }
    }

    IEnumerator bulletRainSpawnerCoroutine(){
        // Bug ? atau feature dari Unity ? 
        // Ketika spawn sampai beberapa milliseconds pertama gak mau gerak di velocity bulletnya (tapi rotatenya udah bener),
        // fixnya dikasih mandatory sleep di awal2.
        // Tapi karena dengan memberi delay di awal2 jadi bagus secara balancing karena gak bikin kaget player
        // dan juga jadi kayak ada touhou ada delay di awal2
        // so two birds in one stone i guess.
        yield return new WaitForSeconds(0.5f);
        while (true){
            for (int i = 0; i < bulletRainSpawnPoint; i++){
                GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
                
                // bullet.transform.SetParent(spawner.transform);
                
                Vector2 currentVel;
                float spawnAngle = (bulletRainAngle + (i * 360/bulletRainSpawnPoint) ) * Mathf.Deg2Rad;
                currentVel.x = (float)(-bulletSpeed * Mathf.Cos(spawnAngle));
                currentVel.y = (float)(-bulletSpeed * Mathf.Sin(spawnAngle));
                bullet.GetComponent<Rigidbody2D>().velocity = currentVel;
            }

            bulletRainAngle += rotationSpeed;
            yield return new WaitForSeconds(bulletRainDelay);
        }
    }

    IEnumerator homingMissileSpawnerCoroutine(){
        yield return new WaitForSeconds(0.5f);
        while (true){
            GameObject missile = Instantiate(missileFab, transform.position, transform.rotation);
            missile.GetComponent<HomingBullet>().targetObj = player;
            missile.transform.SetParent(spawner.transform);
            yield return new WaitForSeconds(homingMissileDelay);
        }
    }

    IEnumerator spinningLaserSpawnerCoroutine(){
        yield return new WaitForSeconds(0.5f);
        GameObject laser = Instantiate(laserFab, transform.position, transform.rotation);
        laser.transform.SetParent(spawner.transform);
        laser.transform.Rotate(0, 0, spinningLaserRotationSpeed*Time.deltaTime);
        while (true){
            yield return new WaitForFixedUpdate();
            if(laser == null){
                break;
            }
            laser.transform.Rotate(0, 0, spinningLaserRotationSpeed*Time.deltaTime);
        }
    }

    protected override void destroySelf(){
        Destroy(this.gameObject);
        Destroy(spawner);
    }
}
