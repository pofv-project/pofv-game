﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBulletMegu : EnemyBehaviour
{
    public int clusterSpawned; 
    public int clusterNumber;
    public float elapsedTime;
    public float delaySpawn;
    // Start is called before the first frame update
    void Start()
    {
        clusterSpawned = 0;
        elapsedTime = delaySpawn;
        clusterNumber = mode;
    }

    // Update is called once per frame
    void Update()
    {
        if (clusterSpawned >= clusterNumber || player == null){
            Destroy(this.gameObject);
        }


        if (elapsedTime >= delaySpawn && player != null){
            float pos = 0.5f;
            for (int i=0;i < 5;i++){
                shootBulletTracking(pos*i);
            }
            elapsedTime = 0;
            clusterSpawned++;
        }
        elapsedTime += Time.deltaTime;
    }
}
