﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrazeDetectorBehaviour : MonoBehaviour
{
    public GameObject player;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Bullet"){
            player.GetComponent<PlayerBehaviour>().increaseSpellCharge(0.5f);
        }
    }
}
