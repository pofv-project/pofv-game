﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterBulletsBehaviour : MonoBehaviour
{
    public GameObject player;
    public GameObject bulletFab;
    public float bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {


           
        //Print("Hello");
    }

    // Update is called once per frame
    void Update()
    {
         //shootBulletTracking();
    }

    public void shootBulletTracking()
    {
        GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
        //homing
        Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
        if (player != null)
        {
            transform.LookAt(player.transform);
            bulletRb2d.velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized*bulletSpeed;
        }
        else
        {
            bulletRb2d.velocity = new Vector2(0, -bulletSpeed);
        }
    }
}
