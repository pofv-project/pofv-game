﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingBullet : MonoBehaviour
{
    public Transform target;
    public GameObject explosion;
    public GameObject targetObj;

    public float speed = 5f;
    public float rotateSpeed = 200f;
    public int hp = 1;

    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if(targetObj != null){
            target = targetObj.GetComponent<Transform>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (hp <= 0){
            destroySelf();
        }

        if(targetObj != null){
            Vector2 dir = (Vector2)target.position-rb.position;
            dir.Normalize();

            float rotateAmount = Vector3.Cross(dir, -transform.up).z;

            rb.angularVelocity = -rotateAmount*rotateSpeed;

            rb.velocity = -transform.up*speed;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "PlayerBullet"){
            hp--;
            Destroy(other.gameObject);
        } else if (other.gameObject.tag == "Player"){
            destroySelf();
        }
    }

    private void destroySelf(){
        if(explosion != null){
            Instantiate(explosion, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
    /*
    private void OnBecameInvisible() {
        Destroy(gameObject);
    }*/
    /*
    void OnCollisionEnter2D(Collision2D coll) {
        Destroy(gameObject);
    }*/
    /*
    private void onCollisionEnter() { 
        Destroy(gameObject); 
    }*/
}
