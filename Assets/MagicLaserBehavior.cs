﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicLaserBehavior : MonoBehaviour
{
    public bool colliderEnabled = true;
    public GameObject player;
    public bool layerSetted;
    public bool isStatic;
    private void Start() {
        if(player != null){
            float AngleRad = Mathf.Atan2(transform.position.y - player.transform.position.y, transform.position.x - player.transform.position.x);
            float AngleDeg = AngleRad*Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, AngleDeg+90);
        }
        if(gameObject.transform.parent != null){
            if(gameObject.transform.parent.gameObject.GetComponent<StaticLaserBehaviour>() != null){
                isStatic = true;
                Transform border = gameObject.transform.parent.parent.parent;
                if(border.gameObject.tag == "Border1"){
                    gameObject.layer = 8;
                } else if (border.gameObject.tag == "Border2"){
                    gameObject.layer = 11;
                }
            }
        }
    }
}
