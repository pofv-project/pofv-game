﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellcardMeguBehaviour : SpellcardBehaviour
{
    public GameObject spell1;
    public GameObject spell2;
    public GameObject spell3;
    private GameObject spell;
    public float spawnDelay;
    //public float timeout;
    //public float timeoutTimePassed;
    // Update is called once per frame

    protected override void instantiateSpell(int mode){
        StartCoroutine(meguSpellSpawnCoroutine());
    }

    IEnumerator meguSpellSpawnCoroutine(){
        yield return new WaitForSeconds(0.2f);
        while(true){
            switch(mode) {
                case 1:
                    spell = spell1;
                    break;
                case 2:
                    spell = spell2;
                    break;
                default:
                    spell = spell3;
                    break;
            }
            spell = Instantiate(spell, transform.position, Quaternion.identity);
            spell.transform.SetParent(this.transform);
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}