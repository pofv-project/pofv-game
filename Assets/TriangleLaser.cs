﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleLaser : MonoBehaviour
{
    public float timeout;
    private float elapsedTime;
    public double angularSpeed;
    // Start is called before the first frame update
    void Start()
    {
        elapsedTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (elapsedTime > timeout){
            Destroy(this.gameObject);
        }
        elapsedTime += Time.deltaTime;
    }

    private void FixedUpdate() {
        transform.Rotate(0,0,(float) angularSpeed*Time.deltaTime);
    }
}
