﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpellcardBehaviour : EnemyBehaviour
{
    public float timeout;
    public float timeoutElapsedTime;

    void Start(){
        instantiateSpell(mode);
        timeoutElapsedTime = 0;
        isSpellCard = true;
    }

    void Update()
    {
        timeoutElapsedTime += Time.deltaTime;

        if (timeoutElapsedTime > timeout) {
            destroySelf();
        }
    }

    abstract protected void instantiateSpell(int mode);
}
