﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public GameObject bulletFab;
    public GameObject missileFab;
    public GameObject laserFab;
    public float delay;
    public float timePassed;
    public GameObject player;
    public float bulletSpeed = 5f;
    public float rotationSpeed = 7;
    public bool tracking = true;
    public bool splitShot = false;
    public bool homingBullet = false;
    public bool isSpellCard = false;
    public float rotateSpeed = 1f;
    private double currentAngle = 0;
    private Vector2 currentVel = new Vector2(0, 0);

    public float hp;
    public int mode;

    public float chargeValue = 0;
    public bool haveEntered = false;

    // Update is called once per frame
    private void Update()
    {
        if (hp <= 0){
            destroySelf();
        }

        timePassed += Time.deltaTime;
        // if (timePassed >= delay && player != null){
        //     int randomAttack = UnityEngine.Random.Range(1,3);
        //     switch(randomAttack){
        //         case 1:
        //             shootBulletRotating();
        //             break;
        //         default:
        //             shootHomingBullet();
        //             break;
        //     }
        //     timePassed = 0;
        // }

        if (timePassed >= delay){
            if (tracking) {
                shootBulletTracking();
            } else if(splitShot) {
                shootBulletRotating();
            } else if (homingBullet) {
                shootHomingBullet();
            }

            timePassed = 0;
        }
    }

    public void resetBullet()
    {
        timePassed = 0;
        currentAngle = 0;
        currentVel = new Vector2(0, 0);
    }

    public void difficultyUp(){
        delay /= 1.2f;
    }

    public void difficultyDown(){
        delay *= 1.2f;
    }

    public void shootHomingBullet(){
        GameObject homingBullet = Instantiate(missileFab, transform.position, transform.rotation);
        homingBullet.GetComponent<HomingBullet>().targetObj = player;
        homingBullet.tag="Bullet";
    }

    public void shootBulletTracking()
    {
        GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
        //homing
        Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
        if (player != null)
        {
            bulletRb2d.velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized*bulletSpeed;
        }
        else
        {
            bulletRb2d.velocity = new Vector2(0, -bulletSpeed);
        }
    }
    public void shootLaserTracking()
    {
        float AngleRad = Mathf.Atan2(transform.position.y - player.transform.position.y, transform.position.x - player.transform.position.x);
        float AngleDeg = AngleRad*Mathf.Rad2Deg;
        GameObject laser = Instantiate(laserFab, transform.position, Quaternion.Euler(0, 0, AngleDeg+90));
        laser.transform.SetParent(this.gameObject.transform.parent);
    }
    public void shootBulletTracking(float positionDelta)
    {
        Vector3 position = new Vector3(transform.position.x, transform.position.y-positionDelta, transform.position.z);
        GameObject bullet = Instantiate(bulletFab, position, transform.rotation);
        //homing
        Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
        if (player != null)
        {
            bulletRb2d.velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized*bulletSpeed;
        }
        else
        {
            bulletRb2d.velocity = new Vector2(0, -bulletSpeed);
        }
    }


    public void shootBulletRotating()
    {
        GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation);
        Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
        currentVel.x = (float)(-bulletSpeed * Math.Cos(currentAngle));
        currentVel.y = (float)(-bulletSpeed * Math.Sin(currentAngle));
        bulletRb2d.velocity = currentVel;
        if (splitShot)
        {
            GameObject bullet2 = Instantiate(bulletFab, transform.position, transform.rotation);
            Physics2D.IgnoreCollision(bullet2.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            Rigidbody2D bulletRb2d2 = bullet2.GetComponent<Rigidbody2D>();
            bulletRb2d2.velocity = currentVel * -1;
        }
        currentAngle += (rotationSpeed/180)*Math.PI;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "PlayerBullet"){
            Destroy(other.gameObject);
            this.hp -= 1;
            if (this.hp == 0){
                player.GetComponent<PlayerBehaviour>().increaseSpellCharge(chargeValue);
                destroySelf();
            }
        }
    }

    private void OnBecameInvisible() {
        Destroy(this.gameObject);
    }

    public void startMoveAndStopCoroutine(){
        StartCoroutine(moveAndStopCoroutine());
    }

    IEnumerator moveAndStopCoroutine(){
        Rigidbody2D rb2d = GetComponent<Rigidbody2D>();
        while (rb2d.velocity.y != 0){
            Vector2 velocity = rb2d.velocity;

            // if (velocity.x < 0){
            //     velocity.x++;
            // } else if (velocity.x > 0){
            //     velocity.x--;
            // }

            if (velocity.y < 0){
                velocity.y++;
            } else if (velocity.y > 0){
                velocity.y--;
            }
            

            rb2d.velocity = velocity;
            yield return new WaitForSeconds(0.4f);
        }

        yield return new WaitForSeconds(1.0f);
        rb2d.velocity = new Vector2(-5, 0);
    }

    protected virtual void destroySelf(){
        Destroy(this.gameObject);
    }
}
