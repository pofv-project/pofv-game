﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryGameManagerBehaviour : GameManagerBehaviour
{
    public GameObject losePanel;
    public GameObject dialogueBox;
    public GameObject skipButton;
    public string nextSceneName;
    // Start is called before the first frame update
    protected override void Start()
    {
        dialogueBox.SetActive(false);
        losePanel.SetActive(false);
        skipButton.SetActive(false);
        base.Start();
        if(!Global.getInstance().haveReadDialogue){
            GameObject.Find("Start").GetComponent<DialogueTrigger>().Starter();
            Global.getInstance().haveReadDialogue = true;
        }
    }

    protected override void Win(int playerNumber){
        if (playerNumber == 1){
            GameObject.Find("End").GetComponent<DialogueTrigger>().Starter();
            Global.getInstance().haveReadDialogue = false;
            StartCoroutine(endDialogueListener());

        } else if (playerNumber == 2){
            losePanel.SetActive(true);
        }
    }

    public void restartLevel(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }

    public void changeToNextLevel(){
        SceneManager.LoadScene(nextSceneName);
        Time.timeScale = 1;
    }

    IEnumerator endDialogueListener(){
        yield return new WaitWhile(() => dialogueBox.activeInHierarchy);
        player1.GetComponent<PlayerBehaviour>().playerInput = false;
        winPanel.SetActive(true);
    }

    public override void togglePauseMenu(){
        if(!gameResolved && !dialogueBox.activeInHierarchy){
            if(!gamePaused){
                gamePaused = true;
                Time.timeScale = 0;
                pausePanel.SetActive(true);
            } else {
                gamePaused = false;
                Time.timeScale = 1;
                pausePanel.SetActive(false);
            }
        }
    }
}
