﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderBehaviour : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "PlayerBullet" || other.gameObject.tag == "Enemy"){
            Destroy(other.gameObject);
        }

        if(other.gameObject.tag == "Bullet"){
            if(other.GetComponent<MagicLaserBehavior>() != null){
                return;
            }
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        //8 = bullet1, 11 = bullet2
        if (other.gameObject.tag == "Bullet"){
            if(other.GetComponent<MagicLaserBehavior>() != null && other.GetComponent<MagicLaserBehavior>().isStatic){
                return;
            }
            if (gameObject.tag == "Border1"){
                other.gameObject.layer = 8;
            } else if (gameObject.tag == "Border2"){
                other.gameObject.layer = 11;
            }
        }
    }

    public void clearAllEnemyAndEnemyBullet(){
        RectTransform border = GetComponent<RectTransform>();
        Rect borderRect = border.rect;

        float leftBorder = border.position.x + borderRect.xMin;
        float rightBorder = border.position.x + borderRect.xMax;
        GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject bullet in bullets){
            if(bullet.transform.position.x >= leftBorder && bullet.transform.position.x <= rightBorder){
                Destroy(bullet);
            }
        }

        foreach(GameObject enemy in enemies){
            if(enemy.transform.position.x >= leftBorder && enemy.transform.position.x <= rightBorder){
                if(enemy.GetComponent<SpellcardBehaviour>() == null){
                    Destroy(enemy);
                }
            }
        }
    }
}
