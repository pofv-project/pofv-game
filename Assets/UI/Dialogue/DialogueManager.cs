using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DialogueManager : MonoBehaviour {

	public TMPro.TextMeshProUGUI nameText;
	public TMPro.TextMeshProUGUI dialogueText;
	
	private Queue<string> sentences;
	private Queue<string> names;

	private bool displayHasStarted;
	public string currSentence;
	// Use this for initialization
	void Start () {

	}

	public void StartDialogue (Dialogue dialogue)
	{
		sentences = new Queue<string>();
		names = new Queue<string>();

		sentences.Clear();
		names.Clear();

		foreach (string name in dialogue.names)
		{
			names.Enqueue(name);
		}

		foreach (string sentence in dialogue.sentences)
		{
			sentences.Enqueue(sentence);
		}

		displayHasStarted = false;
		DisplayNextSentence();
	}

	public void DisplayNextSentence ()
	{
		if (sentences.Count == 0)
		{
			EndDialogue();
			return;
		}

		if(displayHasStarted && !string.Equals(dialogueText.text, currSentence)){
			StopAllCoroutines();
			dialogueText.text = currSentence;
			return;
		}

		currSentence = sentences.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeSentence(currSentence));
	}

	IEnumerator TypeSentence (string sentence)
	{
		dialogueText.text = "";
		displayHasStarted = true;
		nameText.text = names.Dequeue();
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	public void EndDialogue()
	{
		GameObject.Find("DialogBox").SetActive(false);
		GameObject.Find("SkipButton").SetActive(false);
		Time.timeScale = 1;
	}

}
