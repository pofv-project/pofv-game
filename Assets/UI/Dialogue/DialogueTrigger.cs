using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue dialogue;
	public GameObject dialogBox;
	public GameObject skipButton;

	public void Starter() {
		dialogBox.SetActive(true);
		Time.timeScale = 0;
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
		skipButton.SetActive(true);
	}
}
