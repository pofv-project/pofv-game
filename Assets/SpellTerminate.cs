﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellTerminate : MonoBehaviour
{
    public float elapsedTime;
    public float terminationTime;
    // Start is called before the first frame update
    void Start()
    {
        elapsedTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (elapsedTime > terminationTime){
            Destroy(this.gameObject);
        }
        elapsedTime += Time.deltaTime;
    }
}
